# README #

This is a simple motivational game about growing balls. Written in C++ using SDL2.

### How do I download it? ###

* Go to Source -> balls.zip -> view raw
* Then unpack the archive and just run balls.exe
* You need to have Microsoft Visual C++ 2012 Redistributable (x86)

### Can I use the source code? ###

* Sure. If you particularly like any part of it, I'd be happy if you give me credit for it.

*Valentyn Nykoliuk*