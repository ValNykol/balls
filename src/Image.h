#pragma once

#include <SDL.h>

struct SDL_Texture;

struct Image
{
	static SDL_Texture*	loadImage(const std::string& filename);

	static std::unordered_map<std::string, SDL_Texture*>	s_ImageCache;

	Image(const std::string& filename, int rotation = 0, SDL_RendererFlip flip = SDL_FLIP_NONE);

	void				render(int x, int y) const;
	void				renderScaled(int x, int y, float scale) const;

	std::string			m_Filename;
	SDL_Texture*		m_Texture;
	int					m_W;
	int					m_H;
	int					m_Rotation;
	SDL_RendererFlip	m_Flip;
};
