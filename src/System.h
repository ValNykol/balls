#pragma once

#include <SDL.h>

bool systemInit(int width, int height, const char* title);
void systemQuit();

bool pollEvents();
bool handleEvent(const SDL_Event& event);

int systemUpdate();
