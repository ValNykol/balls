#pragma once

#include "Geometry.h"
#include "Actor.h"

struct Interactable : Actor
{
	Interactable(const Image& image, Rect rect);

	virtual void	onClick(Vector2f pos) {}
	virtual void	onRightClick(Vector2f pos) {}
	virtual void	onPress(Vector2f pos) {}
	virtual void	onRightPress(Vector2f pos) {}
	virtual void	onDrag(Vector2f pos) {}
	virtual void	onRightDrag(Vector2f pos) {}
	virtual void	onKeyPress(int key) {}
	//virtual void	onKeyRelease(int key) {}

	void			move(Vector2f pos);

	virtual void	update();
	virtual void	render(const Camera& camera);

	bool	m_LeftHeld;
	bool	m_RightHeld;

	Rect	m_Rect;
};