#include "stdafx.h"

#include "System.h"

#include <SDL_image.h>
#include <SDL_ttf.h>

#include "Globals.h"

SDL_Window* g_Window;
SDL_Renderer* g_Renderer;

int g_LastTime;
bool g_Vsync = false;

bool systemInit(int width, int height, const char* title)
{
	srand(time(nullptr));

	if (SDL_Init(SDL_INIT_EVERYTHING) < 0)
	{
		std::cout << "Couldn't initialize SDL: " << SDL_GetError() << std::endl;
		return false;
	}

	if (IMG_Init(IMG_INIT_PNG) == 0)
	{
		std::cout << "Couldn't initialize SDL_image: " << IMG_GetError() << std::endl;
		return false;
	}

	if (TTF_Init() < 0)
	{
		std::cout << "Couldn't initialize SDL_ttf: " << TTF_GetError() << std::endl;
		return false;
	}

	g_Window = SDL_CreateWindow(title, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, 0);
	if (!g_Window)
	{
		std::cout << "Couldn't create the window: " << SDL_GetError() << std::endl;
		return false;
	}

	g_Vsync = true;
	g_Renderer = SDL_CreateRenderer(g_Window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if (!g_Renderer)
	{
		std::cout << "Error initializing renderer with VSYNC: " << SDL_GetError() << std::endl;
		g_Vsync = false;

		g_Renderer = SDL_CreateRenderer(g_Window, -1, 0);
		if (!g_Renderer)
		{
			std::cout << "Couldn't create a renderer: " << SDL_GetError() << std::endl;
			return false;
		}
	}

	if (SDL_SetRenderDrawBlendMode(g_Renderer, SDL_BLENDMODE_BLEND) < 0)
	{
		std::cout << "Couldn't set draw blend mode: " << SDL_GetError() << std::endl;
	}

	if (SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "2") == SDL_FALSE)
	{
		std::cout << "Couldn't set render scale quality" << std::endl;
	}

	g_LastTime = SDL_GetTicks();

	return true;
}

void systemQuit()
{
	TTF_Quit();
	IMG_Quit();
	SDL_Quit();
}

bool pollEvents()
{
	bool stopGame = false;
	SDL_Event event;

	while (SDL_PollEvent(&event))
	{
		stopGame = handleEvent(event) || stopGame;
	}

	return stopGame;
}

int systemUpdate()
{
	SDL_RenderPresent(g_Renderer);

	int now = SDL_GetTicks();
	int timePassed = now - g_LastTime;

	if (!g_Vsync)
	{
		int waitTime = 1000 / 60 - timePassed;
		if (waitTime > 0)
		{
			SDL_Delay(waitTime);
		}
		now = SDL_GetTicks();
		timePassed = now - g_LastTime;
	}

	g_LastTime = now;

	return timePassed;
}
