#include "stdafx.h"

#include "WateringCan.h"
#include "Globals.h"
#include "Game.h"

const int WATERING_MAX = 45;
const int WATERING_MIN = 35;

WateringCan::WateringCan(Rect rect)
	: Interactable(Image("watering_can.png"), rect)
	, m_Dragging(false)
	, m_Watering(false)
	, m_WateringAngle(0)
{
	m_WateringHoles.push_back(Vector2f(-226,-102));
	m_WateringHoles.push_back(Vector2f(-233,-92 ));
	m_WateringHoles.push_back(Vector2f(-239,-77 ));
	m_WateringHoles.push_back(Vector2f(-240,-62 ));
	m_WateringHoles.push_back(Vector2f(-219,-93 ));
	m_WateringHoles.push_back(Vector2f(-225,-82 ));
}

void WateringCan::onClick(Vector2f pos)
{
	if (!m_Dragging)
	{
		m_Dragging = true;
		m_HoldPos = pos - m_Pos;

		g_Game->onWateringCanGrabbed();
	}
}

void WateringCan::onRightClick(Vector2f pos)
{
	m_Dragging = false;
	m_LeftHeld = false;
}

void WateringCan::onPress(Vector2f pos)
{
}

void WateringCan::onRightPress(Vector2f pos)
{
}

void WateringCan::onDrag(Vector2f pos)
{
}

void WateringCan::onRightDrag(Vector2f pos)
{
}

void WateringCan::onKeyPress(int key)
{
	std::cout << "Watering can onKeyPress " << key << std::endl;
}

void WateringCan::update()
{
	Interactable::update();

	if (m_Dragging)
	{
		auto mousePos = g_Game->getCamera().getWorldPos(g_Mouse.m_Pos);

		auto diff = mousePos - m_Pos - m_HoldPos;

		move(m_Pos + diff);

		m_Watering = g_Mouse.m_LeftHeld;
	}
	else
	{
		m_Watering = false;
	}

	if (m_Watering)
	{
		if (m_WateringAngle < WATERING_MAX)
		{
			m_WateringAngle++;
		}

		g_Game->onWateringCanPushed();
	}
	else
	{
		if (m_WateringAngle > 0)
		{
			m_WateringAngle--;
		}
	}

	m_Image.m_Rotation = -m_WateringAngle;

	if (m_WateringAngle >= WATERING_MIN)
	{
		double radAngle = -m_WateringAngle * M_PI / 180;
		for (auto& hole : m_WateringHoles)
		{
			if (m_WateringAngle < WATERING_MAX)
			{
				ContinueIf(rand() % (WATERING_MAX - WATERING_MIN) >= m_WateringAngle - WATERING_MIN);
			}

			auto r = hole.getLength();
			auto holeAngle = atan2(hole.m_Y, hole.m_X);

			for (int k = 0; k < 2; k++)
			{
				double angleError = -0.02 + 0.04 * ((double)(rand() % 10001) / 10000.0);

				float cosA = cos(holeAngle + radAngle + angleError);
				float sinA = sin(holeAngle + radAngle + angleError);

				Vector2f pos = m_Pos + Vector2f(cosA, sinA) * r;

				double strength = 1.5 + 1.0 * ((double)(rand() % 10001) / 10000.0);
				Vector2f velocity = Vector2f(cosA, sinA) * strength;

				g_Game->addDroplet(pos, velocity);
			}
		}
	}
}
