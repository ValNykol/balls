#pragma once

struct _TTF_Font;
typedef struct _TTF_Font TTF_Font;
struct SDL_Texture;
struct SDL_Color;

struct Font
{
	static TTF_Font*	loadFont(const std::string& filename, int size);
	static std::string	getFontKey(const std::string& filename, int size);
	static std::string	getTextKey(const std::string& text, SDL_Color color);

	static std::unordered_map<std::string, TTF_Font*>	s_FontCache;

	Font(const std::string& filename, int size);

	void	render(int x, int y, const std::string& text, SDL_Color color);

	TTF_Font*										m_Font;
	std::string										m_Filename;
	int												m_Size;
	std::unordered_map<std::string, SDL_Texture*>	m_TextCache;
};
