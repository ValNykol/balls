#include "stdafx.h"

#include "Globals.h"

std::unordered_map<int, bool> g_Keys;
std::unordered_map<int, bool> g_KeysHeld;

bool isKeyHeld(int key)
{
	auto it = g_KeysHeld.find(key);
	if (it == g_KeysHeld.end())
	{
		return false;
	}
	else
	{
		return it->second;
	}
}

bool isKeyTapped(int key)
{
	auto it = g_Keys.find(key);
	if (it == g_Keys.end())
	{
		return false;
	}
	else
	{
		return it->second;
	}
}
