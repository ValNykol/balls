#include "stdafx.h"

#include "Interactable.h"

#include "Globals.h"
#include "Game.h"

Interactable::Interactable(const Image& image, Rect rect)
	: Actor(image, Vector2f((rect.m_X1 + rect.m_X2) / 2, (rect.m_Y1 + rect.m_Y2) / 2))
	, m_Rect(rect)
	, m_LeftHeld(false)
	, m_RightHeld(false)
{
}

void Interactable::move(Vector2f pos)
{
	auto diff = pos - m_Pos;
	m_Rect.m_X1 += diff.m_X;
	m_Rect.m_X2 += diff.m_X;
	m_Rect.m_Y1 += diff.m_Y;
	m_Rect.m_Y2 += diff.m_Y;

	m_Pos = pos;
}

void Interactable::update()
{
	Vector2f mousePos = g_Game->getCamera().getWorldPos(g_Mouse.m_Pos);

	if (g_Mouse.m_Left)
	{
		if (m_Rect.contains(mousePos))
		{
			onPress(mousePos);
			m_LeftHeld = true;
		}
	}

	if (m_LeftHeld)
	{
		onDrag(mousePos);
	}

	if (g_Mouse.m_LeftHeld)
	{
		if (m_LeftHeld && !m_Rect.contains(mousePos))
		{
			m_LeftHeld = false;
		}
	}
	else if (m_LeftHeld)
	{
		onClick(mousePos);
		m_LeftHeld = false;
	}

	if (g_Mouse.m_Right)
	{
		if (m_Rect.contains(mousePos))
		{
			onRightPress(mousePos);
			m_RightHeld = true;
		}
	}

	if (m_RightHeld)
	{
		onRightDrag(mousePos);
	}

	if (g_Mouse.m_RightHeld)
	{
		if (m_RightHeld && !m_Rect.contains(mousePos))
		{
			m_RightHeld = false;
		}
	}
	else if (m_RightHeld)
	{
		onRightClick(mousePos);
		m_RightHeld = false;
	}

	for (auto& it : g_Keys)
	{
		if (it.second)
		{
			onKeyPress(it.first);
		}
	}
}

void Interactable::render(const Camera& camera)
{
	Actor::render(camera);

	/*
	SDL_SetRenderDrawColor(g_Renderer, 0xFF, 0, 0, 0x7F);

	SDL_Rect boxRect;
	boxRect.x = m_Rect.m_X1 - (int)camera.m_Pos.m_X;
	boxRect.y = m_Rect.m_Y1 - (int)camera.m_Pos.m_Y;
	boxRect.w = m_Rect.m_X2 - m_Rect.m_X1;
	boxRect.h = m_Rect.m_Y2 - m_Rect.m_Y1;

	SDL_RenderDrawRect(g_Renderer, &boxRect);
	//*/
}
