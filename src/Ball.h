#pragma once

#include "Geometry.h"
#include "Camera.h"
#include "Image.h"

struct Ball
{
	Ball(Vector2f pos);

	void render(const Camera& camera);

	Image		m_Image;
	Vector2f	m_Pos;
	float		m_Size;
};