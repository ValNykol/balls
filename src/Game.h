#pragma once

#include "Camera.h"

#include "WateringCan.h"
#include "Droplet.h"
#include "Ball.h"
#include "Font.h"

class Game
{
public:
	Game();

	void	update(int dt);
	void	render();

	Camera&	getCamera();

	void	addDroplet(Vector2f pos, Vector2f velocity);

	void	onWateringCanGrabbed();
	void	onWateringCanPushed();

private:
	enum TutorialState
	{
		GRAB_FADE_IN = 0,
		GRAB,
		GRAB_FADE_OUT,
		PUSH_FADE_IN,
		PUSH,
		PUSH_FADE_OUT,
		GROW_FADE_IN,
		GROW
	};

	void	resetGame();

	Camera*	m_Camera;
	WateringCan	m_WateringCan;

	std::vector<Droplet*>	m_Droplets;
	std::vector<Ball>		m_Balls;

	bool	m_Victory;
	int		m_Fade;

	int		m_VictoryCount;

	int		m_TutorialState;
	int		m_TutorialTimer;

	Font	m_Font;
};

extern Game* g_Game;