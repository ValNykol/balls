#include "stdafx.h"

#include "System.h"
#include "Globals.h"
#include "Game.h"

Mouse g_Mouse;

bool handleEvent(const SDL_Event& event)
{
	switch (event.type)
	{
		case SDL_QUIT:
		{
			return true;
		} break;
		case SDL_KEYDOWN:
		{
			int key = event.key.keysym.sym;
			if (key == SDLK_ESCAPE)
			{
				return true;
			}

			if (!isKeyHeld(key))
			{
				g_Keys[key] = true;
				g_KeysHeld[key] = true;
			}
		} break;
		case SDL_KEYUP:
		{
			int key = event.key.keysym.sym;
			g_KeysHeld[key] = false;
		} break;
		case SDL_MOUSEBUTTONDOWN:
		{
			switch (event.button.button)
			{
				case SDL_BUTTON_LEFT:
				{
					if (!g_Mouse.m_LeftHeld)
					{
						g_Mouse.m_Left = true;
						g_Mouse.m_LeftHeld = true;
					}
				} break;
				case SDL_BUTTON_RIGHT:
				{
					if (!g_Mouse.m_RightHeld)
					{
						g_Mouse.m_Right = true;
						g_Mouse.m_RightHeld = true;
					}
				} break;
			}
		} break;
		case SDL_MOUSEBUTTONUP:
		{
			switch (event.button.button)
			{
				case SDL_BUTTON_LEFT:
				{
					g_Mouse.m_LeftHeld = false;
				} break;
				case SDL_BUTTON_RIGHT:
				{
					g_Mouse.m_RightHeld = false;
				} break;
			}
		} break;
		case SDL_MOUSEMOTION:
		{
			g_Mouse.m_Pos.m_X = (float)event.motion.x;
			g_Mouse.m_Pos.m_Y = (float)event.motion.y;
		} break;
	}

	return false;
}

const int SCREEN_WIDTH = 1280;
const int SCREEN_HEIGHT = 720;

int main(int argc, char* argv[])
{
	if (!systemInit(SCREEN_WIDTH, SCREEN_HEIGHT, "Grow balls and do something with your life!"))
	{
		return 0;
	}

	new Game();

	int deltaTime = 0;
	bool stopGame = false;
	while (!stopGame)
	{
		g_Keys.clear();
		g_Mouse.m_Left = false;
		g_Mouse.m_Right = false;

		stopGame = pollEvents();

		g_Game->update(deltaTime);
		g_Game->render();

		deltaTime = systemUpdate();
	}

	systemQuit();
	return 0;
}