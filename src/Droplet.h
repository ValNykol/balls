#pragma once

#include "Geometry.h"

struct Droplet
{
	Droplet(Vector2f pos, Vector2f velocity) : m_Pos(pos), m_LastPos(pos), m_Velocity(velocity) {}

	Vector2f	m_Pos;
	Vector2f	m_LastPos;
	Vector2f	m_Velocity;
};