#pragma once

#include "Geometry.h"

struct Mouse
{
	Mouse() : m_Left(false), m_Right(false), m_LeftHeld(false), m_RightHeld(false) {}

	Vector2f	m_Pos;

	bool	m_Left;
	bool	m_Right;

	bool	m_LeftHeld;
	bool	m_RightHeld;
};
