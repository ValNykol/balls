#include "stdafx.h"

#include "Actor.h"

#include "Camera.h"

Actor::Actor(const Image& image)
	: m_Image(image)
{
}

Actor::Actor(const Image& image, Vector2f pos)
	: m_Image(image)
	, m_Pos(pos)
{
}

void Actor::render(const Camera& camera)
{
	m_Image.render(m_Pos.m_X - m_Image.m_W / 2 - (int)camera.m_Pos.m_X, m_Pos.m_Y - m_Image.m_H / 2 - (int)camera.m_Pos.m_Y);
}
