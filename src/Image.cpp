#include "stdafx.h"

#include "Image.h"

#include <SDL_image.h>
#include "Globals.h"

std::unordered_map<std::string, SDL_Texture*> Image::s_ImageCache;

SDL_Texture* Image::loadImage(const std::string& filename)
{
	auto it = s_ImageCache.find(filename);

	if (it == s_ImageCache.end())
	{
		SDL_Texture* texture = IMG_LoadTexture(g_Renderer, ("images/" + filename).c_str());
		if (!texture)
		{
			std::cout << "Failed to load image '" << filename << "': " << IMG_GetError() << std::endl;
			return nullptr;
		}

		s_ImageCache[filename] = texture;
		return texture;
	}
	else
	{
		return it->second;
	}
}

Image::Image(const std::string& filename, int rotation, SDL_RendererFlip flip)
	: m_Filename(filename)
	, m_Rotation(rotation)
	, m_Flip(flip)
{
	m_Texture = loadImage(filename);

	if (m_Texture)
	{
		SDL_QueryTexture(m_Texture, nullptr, nullptr, &m_W, &m_H);
	}
}

void Image::render(int x, int y) const
{
	SDL_Rect dstRect = {x, y, m_W, m_H};

	SDL_RenderCopyEx(g_Renderer, m_Texture, nullptr, &dstRect, m_Rotation, nullptr, m_Flip);
}

void Image::renderScaled(int x, int y, float scale) const
{
	SDL_Rect dstRect = {x, y, m_W * scale, m_H * scale};

	SDL_RenderCopyEx(g_Renderer, m_Texture, nullptr, &dstRect, m_Rotation, nullptr, m_Flip);
}
