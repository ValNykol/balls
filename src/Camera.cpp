#include "stdafx.h"

#include "Camera.h"

#include "Globals.h"

Camera::Camera(const Vector2f* target)
	: m_Target(target)
{
	if (target)
	{
		m_Pos = Vector2f(target->m_X - SCREEN_WIDTH / 2, target->m_Y - SCREEN_HEIGHT / 2);
	}
}

Vector2f Camera::getWorldPos(const Vector2f& cameraPos) const
{
	return cameraPos + m_Pos;
}

Vector2f Camera::getCameraPos(const Vector2f& worldPos) const
{
	return worldPos - m_Pos;
}

const float CAMERA_SMOOTHING = 0.1f;
const float MOUSE_LOOK_AHEAD = 0.2f;

void Camera::update()
{
	if (m_Target)
	{
		m_Pos += (*m_Target - Vector2f(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2) + (g_Mouse.m_Pos + m_Pos - *m_Target) * MOUSE_LOOK_AHEAD - m_Pos) * CAMERA_SMOOTHING;
	}
}
