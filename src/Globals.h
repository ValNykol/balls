#pragma once

#include "Mouse.h"

struct SDL_Renderer;

extern SDL_Renderer* g_Renderer;

extern const int SCREEN_WIDTH;
extern const int SCREEN_HEIGHT;

extern Mouse g_Mouse;
extern std::unordered_map<int, bool> g_Keys;
extern std::unordered_map<int, bool> g_KeysHeld;

bool isKeyHeld(int key);
bool isKeyTapped(int key);
