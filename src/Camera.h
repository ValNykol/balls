#pragma once

#include "Geometry.h"

struct Camera
{
	Camera(const Vector2f* target);

	Vector2f	getWorldPos(const Vector2f& cameraPos) const;
	Vector2f	getCameraPos(const Vector2f& worldPos) const;

	void		update();

	const Vector2f*	m_Target;

	Vector2f		m_Pos;
};
