#pragma once

#include "Interactable.h"

struct WateringCan : Interactable
{
	WateringCan(Rect rect = Rect());

	virtual void	onClick(Vector2f pos);
	virtual void	onRightClick(Vector2f pos);
	virtual void	onPress(Vector2f pos);
	virtual void	onRightPress(Vector2f pos);
	virtual void	onDrag(Vector2f pos);
	virtual void	onRightDrag(Vector2f pos);
	virtual void	onKeyPress(int key);

	virtual void	update();

	bool		m_Dragging;
	Vector2f	m_HoldPos;

	bool		m_Watering;
	int			m_WateringAngle;

	std::vector<Vector2f>	m_WateringHoles;
};
