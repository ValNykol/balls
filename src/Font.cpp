#include "stdafx.h"

#include "Font.h"

#include <SDL_ttf.h>

#include "Globals.h"

std::unordered_map<std::string, TTF_Font*> Font::s_FontCache;

TTF_Font* Font::loadFont(const std::string& filename, int size)
{
	auto key = getFontKey(filename, size);

	auto it = s_FontCache.find(key);

	if (it == s_FontCache.end())
	{
		TTF_Font* font = TTF_OpenFont(("fonts/" + filename).c_str(), size);
		if (!font)
		{
			std::cout << "Failed to load font '" << filename << "': " << TTF_GetError() << std::endl;
			return nullptr;
		}

		s_FontCache[key] = font;
		return font;
	}
	else
	{
		return it->second;
	}
}

std::string Font::getFontKey(const std::string& filename, int size)
{
	std::string filenameLower = filename;
	std::transform(filenameLower.begin(), filenameLower.end(), filenameLower.begin(), ::tolower);
	return filenameLower + std::to_string(size);
}

std::string Font::getTextKey(const std::string& text, SDL_Color color)
{
	std::stringstream stream;
	stream << std::hex << color.r << color.g << color.b;
	return text + stream.str();
}

Font::Font(const std::string& filename, int size)
	: m_Filename(filename)
	, m_Size(size)
{
	m_Font = loadFont(filename, size);
}

void Font::render(int x, int y, const std::string& text, SDL_Color color)
{
	if (m_Font)
	{
		int alpha = color.a;

		auto key = getTextKey(text, color);

		SDL_Texture* texture;

		auto it = m_TextCache.find(key);
	
		if (it == m_TextCache.end())
		{
			auto surface = TTF_RenderUTF8_Blended(m_Font, text.c_str(), color);
			texture = SDL_CreateTextureFromSurface(g_Renderer, surface);
			if (texture)
			{
				m_TextCache[key] = texture;
			}

			SDL_FreeSurface(surface);
		}
		else
		{
			texture = it->second;
		}

		if (texture)
		{
			int w, h;
			SDL_QueryTexture(texture, nullptr, nullptr, &w, &h);

			SDL_Rect dstrect;
			dstrect.x = x - w / 2;
			dstrect.y = y - h / 2;
			dstrect.w = w;
			dstrect.h = h;

			SDL_SetTextureAlphaMod(texture, alpha);

			SDL_RenderCopy(g_Renderer, texture, nullptr, &dstrect);
		}
	}
}
