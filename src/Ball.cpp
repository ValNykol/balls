#include "stdafx.h"

#include "Ball.h"

Ball::Ball(Vector2f pos)
	: m_Image("ball.png")
	, m_Pos(pos)
	, m_Size(0.15f)
{
}

void Ball::render(const Camera& camera)
{
	m_Image.renderScaled(m_Pos.m_X - m_Size * m_Image.m_W / 2 - (int)camera.m_Pos.m_X, m_Pos.m_Y - m_Size * m_Image.m_H / 2 - (int)camera.m_Pos.m_Y, m_Size);
}
