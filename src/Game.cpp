#include "stdafx.h"

#include "Game.h"

#include "System.h"
#include "Globals.h"

Game* g_Game;

Game::Game()
	: m_Font("LondonBetween.ttf", 24)
	, m_VictoryCount(0)
	, m_TutorialState(GRAB_FADE_IN)
	, m_TutorialTimer(0)
{
	g_Game = this;

	m_Camera = new Camera(nullptr);

	resetGame();
}

const float	BALL_MAX_SIZE = 1.0f;

const int	TUTORIAL_FADE_DURATION = 60;

void Game::update(int dt)
{
	m_WateringCan.update();

	std::vector<Droplet*> dropletsToKeep;

	for (auto droplet : m_Droplets)
	{
		droplet->m_LastPos = droplet->m_Pos;
		droplet->m_Pos += droplet->m_Velocity;
		droplet->m_Velocity.m_Y += 0.3f;

		bool consumed = false;

		for (auto& ball : m_Balls)
		{
			auto dist = (droplet->m_Pos - ball.m_Pos).getLength();

			if (dist < ball.m_Size * 62)
			{
				ball.m_Size += 0.0005f;
				if (ball.m_Size > BALL_MAX_SIZE)
				{
					ball.m_Size = BALL_MAX_SIZE;
				}
				else
				{
					ball.m_Pos.m_Y -= 0.01f;
				}

				consumed = true;
				break;
			}
		}

		if (consumed || droplet->m_Pos.m_Y >= SCREEN_HEIGHT - 100)
		{
			delete droplet;
		}
		else
		{
			dropletsToKeep.push_back(droplet);
		}
	}

	m_Droplets = dropletsToKeep;

	if (m_Victory)
	{
		m_Fade += 5;
		if (m_Fade > 0xFF)
		{
			m_Fade = 0xFF;
		}

		if (isKeyTapped(SDLK_r))
		{
			resetGame();
		}
	}
	else
	{
		bool victory = true;
		for (auto& ball : m_Balls)
		{
			if (ball.m_Size < BALL_MAX_SIZE)
			{
				victory = false;
				break;
			}
		}

		if (victory)
		{
			m_Victory = true;
			m_VictoryCount++;
		}
	}

	switch (m_TutorialState)
	{
	case GRAB_FADE_IN:
	case GRAB_FADE_OUT:
	case PUSH_FADE_IN:
	case PUSH_FADE_OUT:
	case GROW_FADE_IN:
		{
			m_TutorialTimer++;
			if (m_TutorialTimer >= TUTORIAL_FADE_DURATION)
			{
				m_TutorialState++;
				m_TutorialTimer = 0;
			}
		} break;
	}

	m_Camera->update();
}

void Game::render()
{
	SDL_SetRenderDrawColor(g_Renderer, 0xDF, 0xDF, 0xDF, 0xFF);
	SDL_RenderClear(g_Renderer);

	SDL_SetRenderDrawColor(g_Renderer, 0x4C, 0x35, 0, 0xFF);
	{
		SDL_Rect rect;
		rect.x = 0;
		rect.w = SCREEN_WIDTH;
		rect.y = SCREEN_HEIGHT - 100;
		rect.h = 100;

		SDL_RenderFillRect(g_Renderer, &rect);
	}

	SDL_SetRenderDrawColor(g_Renderer, 0, 0x90, 0, 0xFF);
	{
		SDL_Rect rect;
		rect.x = 0;
		rect.w = SCREEN_WIDTH;
		rect.y = SCREEN_HEIGHT - 110;
		rect.h = 10;

		SDL_RenderFillRect(g_Renderer, &rect);
	}

	for (auto& ball : m_Balls)
	{
		ball.render(*m_Camera);
	}

	m_WateringCan.render(*m_Camera);

	SDL_SetRenderDrawColor(g_Renderer, 0, 0, 0xFF, 0xFF);
	for (auto droplet : m_Droplets)
	{
		int x1 = droplet->m_Pos.m_X - (int)m_Camera->m_Pos.m_X;
		int y1 = droplet->m_Pos.m_Y - (int)m_Camera->m_Pos.m_Y;
		int x2 = droplet->m_LastPos.m_X - (int)m_Camera->m_Pos.m_X;
		int y2 = droplet->m_LastPos.m_Y - (int)m_Camera->m_Pos.m_Y;

		SDL_RenderDrawLine(g_Renderer, x1, y1, x2, y2);
	}

	{
		std::string tutorialText;

		switch (m_TutorialState)
		{
		case GRAB_FADE_IN:
		case GRAB:
		case GRAB_FADE_OUT:
			{
				tutorialText = "Left click the watering can to grab it.";
			} break;
			
		case PUSH_FADE_IN:
		case PUSH:
		case PUSH_FADE_OUT:
			{
				tutorialText = "Hold left mouse button to pour water.";
			} break;
			
		case GROW_FADE_IN:
		case GROW:
			{
				tutorialText = "Grow balls.";
			} break;
		}

		int alpha = 0xFF;

		switch (m_TutorialState)
		{
		case GRAB_FADE_IN:
		case PUSH_FADE_IN:
		case GROW_FADE_IN:
			{
				alpha = (m_TutorialTimer * 0xFF) / TUTORIAL_FADE_DURATION;
			} break;
			
		case GRAB_FADE_OUT:
		case PUSH_FADE_OUT:
			{
				alpha = 0xFF - (m_TutorialTimer * 0xFF) / TUTORIAL_FADE_DURATION;
			} break;
		}

		SDL_Color color;
		color.r = 0;
		color.g = 0;
		color.b = 0;
		color.a = alpha;

		m_Font.render(2 * SCREEN_WIDTH / 3, SCREEN_HEIGHT / 3, tutorialText, color);
	}

	if (m_Victory)
	{
		SDL_SetRenderDrawColor(g_Renderer, 0, 0, 0, m_Fade);
		SDL_Rect rect;
		rect.x = 0;
		rect.y = 0;
		rect.w = SCREEN_WIDTH;
		rect.h = SCREEN_HEIGHT;

		SDL_RenderFillRect(g_Renderer, &rect);

		SDL_Color color;
		color.r = 0xFF;
		color.g = 0xFF;
		color.b = 0xFF;
		color.a = m_Fade;

		m_Font.render(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2 - 100, "You win!", color);
		if (m_VictoryCount < 3)
		{
			m_Font.render(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2 - 50, "Press R to restart", color);
		}
		else
		{
			m_Font.render(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2 - 50, "You should probably stop playing and", color);
			m_Font.render(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2 - 20, "grow some balls in real life", color);
			m_Font.render(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2 + 50, "Press R to restart", color);
		}
	}
}

Camera& Game::getCamera()
{
	return *m_Camera;
}

void Game::addDroplet(Vector2f pos, Vector2f velocity)
{
	m_Droplets.push_back(new Droplet(pos, velocity));
}

void Game::resetGame()
{
	m_WateringCan = WateringCan(Rect(100, 450, 100, 400));

	m_Victory = false;
	m_Fade = 0;

	m_Balls.clear();

	while (m_Balls.size() < 5)
	{
		int x = 100 + rand() % (SCREEN_WIDTH - 500);
		int y = SCREEN_HEIGHT - 104 + rand() % 3;

		bool bad = false;
		for (auto& ball : m_Balls)
		{
			if (abs(x - ball.m_Pos.m_X) < 120)
			{
				bad = true;
				break;
			}
		}

		if (!bad)
		{
			m_Balls.push_back(Ball(Vector2f(x, y)));
		}
	}
}

void Game::onWateringCanGrabbed()
{
	if (m_TutorialState < GRAB_FADE_OUT)
	{
		m_TutorialState = GRAB_FADE_OUT;
		m_TutorialTimer = 0;
	}
}

void Game::onWateringCanPushed()
{
	if (m_TutorialState >= PUSH_FADE_IN && m_TutorialState < PUSH_FADE_OUT)
	{
		m_TutorialState = PUSH_FADE_OUT;
		m_TutorialTimer = 0;
	}
}
