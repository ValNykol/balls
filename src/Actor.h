#pragma once

#include "Geometry.h"
#include "Image.h"
#include "Camera.h"

struct Actor
{
	Actor(const Image& image);
	Actor(const Image& image, Vector2f pos);

	virtual void	update() {}
	virtual void	render(const Camera& camera);

	Vector2f	m_Pos;
	Image		m_Image;
};
