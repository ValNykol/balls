#pragma once

struct Vector2f
{
	Vector2f() : m_X(0.0f), m_Y(0.0f) {}
	Vector2f(float x, float y) : m_X(x), m_Y(y) {}

	Vector2f operator+(const Vector2f& other) const
	{
		return Vector2f(m_X + other.m_X, m_Y + other.m_Y);
	}

	Vector2f operator-(const Vector2f& other) const
	{
		return Vector2f(m_X - other.m_X, m_Y - other.m_Y);
	}

	Vector2f& operator+=(const Vector2f& other)
	{
		*this = *this + other;
		return *this;
	}

	Vector2f& operator-=(const Vector2f& other)
	{
		*this = *this - other;
		return *this;
	}

	Vector2f operator*(float k) const
	{
		return Vector2f(m_X * k, m_Y * k);
	}

	Vector2f& operator*=(float k)
	{
		*this = *this * k;
		return *this;
	}

	float getLengthSqr() const
	{
		return m_X * m_X + m_Y * m_Y;
	}

	float getLength() const
	{
		return sqrt(getLengthSqr());
	}

	float m_X;
	float m_Y;
};

struct Rect
{
	Rect() : m_X1(0.0f), m_X2(0.0f), m_Y1(0.0f), m_Y2(0.0f) {}
	Rect(float x1, float x2, float y1, float y2) : m_X1(x1), m_X2(x2), m_Y1(y1), m_Y2(y2) {}

	bool contains(Vector2f point) const
	{
		return (m_X1 <= point.m_X &&
				m_X2 >= point.m_X &&
				m_Y1 <= point.m_Y &&
				m_Y2 >= point.m_Y);
	}

	bool collidesWithRect(const Rect& other) const
	{
		return (m_X1 < other.m_X2 &&
				m_X2 > other.m_X1 &&
				m_Y1 < other.m_Y2 &&
				m_Y2 > other.m_Y1);
	}

	float m_X1;
	float m_X2;
	float m_Y1;
	float m_Y2;
};
